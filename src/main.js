import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

new Vue({
        theme: {
            themes: {
                light: {
                    primary: '#230C33',
                    secondary: '#230C33',
                    accent: '#230C33',
                    error: '#230C33',
                },
                dark: {
                    primary: '#230C33',
                },
            },
        },
        methods: {
            log: function (e) {
                console.log(e.currentTarget);
                console.log(e);
            }
        },
        vuetify,
        render: h => h(App)
    }
).$mount('#app');
