import Dictate from 'dictate.js/lib/dictate.js'
import Recorder from 'dictate.js/lib/recorder.js'
import Transcription from './transcription.js'

//var transcription;
let dictate;
let transcription;
export default {
    finished: false,
    init() {
        // TODO: fix this and that when I have more time
        let that = this
        transcription = new Transcription()
        dictate = new Dictate(Recorder, {
            server : "wss://bark.phon.ioc.ee:8443/dev/duplex-speech-api/ws/speech",
            serverStatus : "wss://bark.phon.ioc.ee:8443/dev/duplex-speech-api/ws/status",
            recorderWorkerPath : '/recorderWorker.js',
            onReadyForSpeech : function() {
                console.log("ready fro speech")
            },
            onEndOfSpeech : function() {
                console.log("end of speech")
            },
            onEndOfSession : function() {
                console.log("session ended")
            },
            onServerStatus : function(json) {
                console.log(json.num_workers_available + ':' + json.num_requests_processed);
            },
            onPartialResults : function(result) {
                console.log("partial result", result)
                transcription.add(result[0].transcript, false)
            },
            onResults : function(result) {
                console.log("full result", result)
                transcription.add(result[0].transcript, false)
                that.finished = true;
            },
            onError : function(code, data) {
                console.log("error", code, data);
                dictate.cancel();
            },
            onEvent : function(code, data) {
                console.log("event", code, data);
            }
        });
        dictate.init()
    },
    start() {
        dictate.startListening();
        this.finished = false;
    },
    stop() {
        dictate.stopListening();
    },
    getResults() {
        return transcription.toString();
    }
}
