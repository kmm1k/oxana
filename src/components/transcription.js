var Transcription = function() {
    var index = 0;
    var list = [];

    this.add = function(text, isFinal) {
        list[index] = text;
        if (isFinal) {
            index++;
        }
    }

    this.toString = function() {
        return list.join('. ');
    }
}

export default Transcription;
